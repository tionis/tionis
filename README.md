### Hi there 👋, I am tionis
#### I am an internet computing student in germany
![I am a internet computing student in germany](https://unsplash.com/photos/m_HRfLhgABo/download?force=true)

There's not much to know about me. I'm currently writing on a few tiny projects next to my studies and am quite fond of stuff like decentralization and other ways to make the internet better for everyone.

Skills: GO / JAVA / JS / HTML / CSS

- 🔭 I’m currently working on a discord bot for online roleplay games. 
- 🌱 I’m currently learning React 
- 💬 Ask me about anything 
- 📫 How to reach me: per mail at tionis@tasadar.net 
- ⚡ Fun fact: I have no idea what I'm doing 


[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/protonmail.svg' alt='github' height='40'>](mailto://tionis@tasadar.net)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/icloud.svg' alt='website' height='40'>](https://tasadar.net)  

[![trophy](https://github-profile-trophy.vercel.app/?username=tionis)](https://github.com/ryo-ma/github-profile-trophy)

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=tionis)](https://github.com/anuraghazra/github-readme-stats)

![GitHub stats](https://github-readme-stats.vercel.app/api?username=tionis&show_icons=true&count_private=true)  

![Profile views](https://gpvc.arturio.dev/tionis)  
